# nes

based on https://archive.org/details/no-intro_romsets (20210209-071116)

ROM not added :
```
.
├── [ 20K]  Virtual Console
│   ├── [141K]  Adventures of Bayou Billy, The (USA) (Virtual Console).zip
│   ├── [ 41K]  Adventures of Lolo 2 (Europe) (Virtual Console).zip
│   ├── [ 41K]  Adventures of Lolo 2 (USA) (Virtual Console).zip
│   ├── [ 35K]  Adventures of Lolo (Europe) (Virtual Console).zip
│   ├── [ 36K]  Adventures of Lolo (USA) (Virtual Console).zip
│   ├── [241K]  Akumajou Densetsu (Japan) (Virtual Console).zip
│   ├── [ 28K]  Atlantis no Nazo (Japan) (Virtual Console, Switch Online).zip
│   ├── [ 16K]  Balloon Fight (Europe) (Virtual Console).zip
│   ├── [123K]  Baseball Simulator 1.000 (USA) (Virtual Console).zip
│   ├── [ 81K]  Blades of Steel (Europe) (Virtual Console).zip
│   ├── [ 81K]  Blades of Steel (USA) (Virtual Console).zip
│   ├── [148K]  Blaster Master (Japan) (Virtual Console).zip
│   ├── [155K]  Blaster Master (USA, Europe) (Virtual Console).zip
│   ├── [ 49K]  Bubble Bobble (Europe) (Virtual Console).zip
│   ├── [ 49K]  Bubble Bobble (USA) (Virtual Console).zip
│   ├── [ 65K]  Castlevania (Europe) (Virtual Console).zip
│   ├── [242K]  Castlevania III - Dracula's Curse (Europe) (Virtual Console).zip
│   ├── [240K]  Castlevania III - Dracula's Curse (USA) (Virtual Console).zip
│   ├── [ 65K]  Castlevania (USA) (Rev 1) (Virtual Console).zip
│   ├── [124K]  Choujin - Ultra Baseball (Japan) (Virtual Console).zip
│   ├── [ 26K]  City Connection (Europe) (Virtual Console).zip
│   ├── [ 20K]  City Connection (Japan) (Virtual Console, Switch Online).zip
│   ├── [ 16K]  Clu Clu Land (World) (Virtual Console).zip
│   ├── [ 25K]  Dig Dug II - Trouble in Paradise (USA, Europe) (Virtual Console).zip
│   ├── [ 19K]  Donkey Kong - Original Edition (Europe) (Virtual Console) (NTSC).zip
│   ├── [ 19K]  Donkey Kong - Original Edition (USA) (Virtual Console).zip
│   ├── [137K]  Double Dragon (Europe) (Virtual Console).zip
│   ├── [125K]  Downtown - Nekketsu Monogatari (Japan) (Virtual Console).zip
│   ├── [ 62K]  Dragon Buster (Japan) (Virtual Console).zip
│   ├── [227K]  EarthBound Beginnings (USA, Europe) (Virtual Console).zip
│   ├── [ 21K]  Elevator Action (Japan) (Rev 1) (Virtual Console).zip
│   ├── [ 21K]  Elevator Action (USA) (Virtual Console).zip
│   ├── [ 18K]  Excitebike (Europe) (Virtual Console).zip
│   ├── [ 18K]  Excitebike (Japan, USA) (Virtual Console).zip
│   ├── [108K]  Famicom Wars (Japan) (Virtual Console).zip
│   ├── [325K]  Final Fantasy III (Japan) (3DS Virtual Console).zip
│   ├── [325K]  Final Fantasy III (Japan) (Wii U Virtual Console).zip
│   ├── [325K]  Final Fantasy III (Japan) (Wii Virtual Console).zip
│   ├── [165K]  Final Fantasy II (Japan) (3DS Virtual Console).zip
│   ├── [165K]  Final Fantasy II (Japan) (Wii and Wii U Virtual Console).zip
│   ├── [143K]  Final Fantasy (Japan) (Rev 1) (3DS Virtual Console).zip
│   ├── [143K]  Final Fantasy (Japan) (Rev 1) (Wii and Wii U Virtual Console).zip
│   ├── [144K]  Final Fantasy (USA) (Virtual Console).zip
│   ├── [240K]  Fire Emblem Gaiden (Japan) (Virtual Console).zip
│   ├── [164K]  Flying Warriors (USA) (Virtual Console).zip
│   ├── [ 14K]  Formation Z (Japan) (Rev A) (Virtual Console).zip
│   ├── [158K]  Ganbare Goemon 2 (Japan) (Virtual Console).zip
│   ├── [318K]  Ganbare Goemon Gaiden - Kieta Ougon Kiseru (Japan) (Rev 1) (Virtual Console).zip
│   ├── [109K]  Ganbare Goemon! - Karakuri Douchuu (Japan) (Virtual Console).zip
│   ├── [143K]  Gekikame Ninja Den (Japan) (Virtual Console).zip
│   ├── [150K]  Getsu Fuuma Den (Japan) (Virtual Console).zip
│   ├── [ 86K]  Hanjuku Hero (Japan) (Virtual Console).zip
│   ├── [123K]  Hebereke (Japan) (Virtual Console).zip
│   ├── [158K]  Hiryuu no Ken II - Dragon no Tsubasa (Japan) (Virtual Console).zip
│   ├── [ 27K]  Ice Hockey (Europe) (Virtual Console).zip
│   ├── [231K]  Joy Mech Fight (Japan) (Virtual Console).zip
│   ├── [ 26K]  Kage no Densetsu (Japan) (Virtual Console).zip
│   ├── [ 63K]  Kid Icarus (USA, Europe) (Rev 1) (Virtual Console).zip
│   ├── [ 41K]  King's Knight (Japan) (Virtual Console).zip
│   ├── [ 40K]  King's Knight (USA) (Virtual Console).zip
│   ├── [ 26K]  Legend of Kage, The (USA) (Virtual Console).zip
│   ├── [ 64K]  Legend of Zelda, The (Europe) (Rev 1) (Virtual Console).zip
│   ├── [ 64K]  Legend of Zelda, The (USA) (Rev 1) (Virtual Console).zip
│   ├── [ 88K]  Life Force - Salamander (Europe) (Virtual Console).zip
│   ├── [ 16K]  Lode Runner (Japan) (Virtual Console).zip
│   ├── [ 16K]  Lode Runner (USA, Europe) (Virtual Console).zip
│   ├── [ 25K]  Mach Rider (Europe) (Virtual Console).zip
│   ├── [ 25K]  Mach Rider (Japan, USA) (Rev 1) (Virtual Console).zip
│   ├── [ 25K]  Mach Rider (Japan, USA) (Virtual Console).zip
│   ├── [133K]  Mad City (Japan) (Virtual Console).zip
│   ├── [ 55K]  Mappy-Land (USA, Europe) (Virtual Console).zip
│   ├── [185K]  Mario Open Golf (Japan) (Rev 1) (Virtual Console).zip
│   ├── [132K]  Mega Man 2 (Europe) (Virtual Console).zip
│   ├── [132K]  Mega Man 2 (USA) (Virtual Console).zip
│   ├── [225K]  Mega Man 3 (Europe) (Rev 1) (Virtual Console).zip
│   ├── [217K]  Mega Man 3 (USA) (Virtual Console).zip
│   ├── [280K]  Mega Man 4 (Europe) (Virtual Console).zip
│   ├── [287K]  Mega Man 4 (USA) (Virtual Console).zip
│   ├── [261K]  Mega Man 5 (Europe) (Virtual Console).zip
│   ├── [260K]  Mega Man 5 (USA) (Virtual Console).zip
│   ├── [ 77K]  Mega Man (USA) (Virtual Console).zip
│   ├── [ 41K]  Meikyuu Kumikyoku - Milon no Daibouken (Japan) (Virtual Console).zip
│   ├── [325K]  Metal Max (Japan) (Virtual Console).zip
│   ├── [642K]  Metal Slader Glory (Japan) (Virtual Console).zip
│   ├── [ 59K]  Metroid (Europe) (Virtual Console).zip
│   ├── [ 59K]  Metroid (USA) (Virtual Console).zip
│   ├── [ 29K]  Mighty Bomb Jack (Japan) (Rev 1) (Virtual Console).zip
│   ├── [ 40K]  Milon's Secret Castle (USA) (Virtual Console).zip
│   ├── [ 80K]  Moero!! Pro Yakyuu (Japan) (Rev 3) (Virtual Console).zip
│   ├── [ 61K]  Moero TwinBee - Cinnamon Hakase o Sukue! (Japan) (Virtual Console).zip
│   ├── [200K]  Mother (Japan) (Virtual Console).zip
│   ├── [ 67K]  Nekketsu Kouha Kunio-kun (Japan) (Virtual Console).zip
│   ├── [107K]  Nekketsu Koukou Dodgeball-bu (Japan) (Virtual Console).zip
│   ├── [129K]  Nekketsu Koukou Dodgeball-bu - Soccer Hen (Japan) (Virtual Console).zip
│   ├── [184K]  NES Open Tournament Golf (Europe) (Virtual Console).zip
│   ├── [184K]  NES Open Tournament Golf (USA) (Virtual Console).zip
│   ├── [164K]  NES Play Action Football (USA) (Virtual Console).zip
│   ├── [163K]  Ninja Gaiden III - The Ancient Ship of Doom (USA) (Wii U Virtual Console).zip
│   ├── [163K]  Ninja Gaiden III - The Ancient Ship of Doom (USA) (Wii Virtual Console).zip
│   ├── [162K]  Ninja Gaiden II - The Dark Sword of Chaos (USA) (Virtual Console).zip
│   ├── [151K]  Ninja Gaiden (Japan) (Wii U Virtual Console).zip
│   ├── [ 20K]  Ninja Jajamaru-kun (World) (Ja) (Virtual Console).zip
│   ├── [151K]  Ninja Ryuuken Den (Japan) (Wii Virtual Console).zip
│   ├── [ 11K]  Pac-Man (USA) (Virtual Console).zip
│   ├── [188K]  Princess Tomato in the Salad Kingdom (USA) (Virtual Console).zip
│   ├── [130K]  Punch-Out!! (Europe) (Virtual Console).zip
│   ├── [130K]  Punch-Out!! (Japan, USA) (Virtual Console).zip
│   ├── [ 69K]  Renegade (USA) (Virtual Console).zip
│   ├── [132K]  Rockman 2 - Dr. Wily no Nazo (Japan) (Virtual Console).zip
│   ├── [223K]  Rockman 3 - Dr. Wily no Saigo! (Japan) (Virtual Console).zip
│   ├── [282K]  Rockman 4 - Aratanaru Yabou!! (Japan) (Virtual Console).zip
│   ├── [277K]  Rockman 5 - Blues no Wana! (Japan) (Virtual Console).zip
│   ├── [ 77K]  Rockman (Japan) (Virtual Console).zip
│   ├── [184K]  Salad no Kuni no Tomato Hime (Japan) (Virtual Console).zip
│   ├── [135K]  S.C.A.T. - Special Cybernetic Attack Team (USA, Europe) (Virtual Console).zip
│   ├── [ 23K]  Seicross (Japan) (Virtual Console).zip
│   ├── [138K]  Shadow of the Ninja (USA, Europe) (Virtual Console).zip
│   ├── [138K]  Shadow Warriors - Ninja Gaiden (Europe) (Virtual Console).zip
│   ├── [ 31K]  Sky Kid (Japan) (Virtual Console).zip
│   ├── [ 21K]  Spelunker (Japan) (Virtual Console).zip
│   ├── [ 21K]  Spelunker (USA, Europe) (Virtual Console).zip
│   ├── [ 23K]  Star Luster (Japan) (Virtual Console).zip
│   ├── [251K]  StarTropics (Europe) (Virtual Console).zip
│   ├── [251K]  StarTropics (USA) (Virtual Console).zip
│   ├── [ 56K]  Stinger (USA) (Virtual Console).zip
│   ├── [121K]  Street Gangs (Europe) (Virtual Console).zip
│   ├── [154K]  Sugoro Quest - Dice no Senshitachi (Japan) (Virtual Console).zip
│   ├── [172K]  Super Chinese 2 - Dragon Kid (Japan) (Virtual Console).zip
│   ├── [101K]  Super Dodge Ball (USA) (Virtual Console).zip
│   ├── [ 31K]  Super Mario Bros. - 25th Anniversary (Australia) (Virtual Console).zip
│   ├── [ 31K]  Super Mario Bros. - 25th Anniversary (Japan) (En) (Virtual Console).zip
│   ├── [ 76K]  Super Mario Bros. 2 (Europe) (Virtual Console).zip
│   ├── [ 76K]  Super Mario Bros. 2 (Japan) (Virtual Console).zip
│   ├── [ 76K]  Super Mario Bros. 2 (USA) (Rev 1) (Virtual Console).zip
│   ├── [221K]  Super Mario Bros. 3 (Europe) (Virtual Console).zip
│   ├── [221K]  Super Mario Bros. 3 (Japan) (Rev 1) (Virtual Console).zip
│   ├── [221K]  Super Mario Bros. 3 (USA) (Rev 1) (Virtual Console).zip
│   ├── [ 67K]  Takeshi no Chousenjou (Japan) (Virtual Console).zip
│   ├── [ 98K]  Tantei Jinguuji Saburou - Yokohamakou Renzoku Satsujin Jiken (Japan) (Virtual Console).zip
│   ├── [ 32K]  Tatakae! Chou Robot Seimeitai Transformers - Convoy no Nazo (Japan) (Virtual Console).zip
│   ├── [ 92K]  Tecmo Bowl (World) (Rev 1) (Virtual Console, Switch Online).zip
│   ├── [142K]  Teenage Mutant Hero Turtles (Europe) (Virtual Console).zip
│   ├── [142K]  Teenage Mutant Ninja Turtles (USA) (Virtual Console).zip
│   ├── [ 42K]  Tsuppari Oozumou (Japan) (Virtual Console).zip
│   ├── [122K]  Ufouria - The Saga (USA, Europe) (Wii U Virtual Console).zip
│   ├── [122K]  Ufouria - The Saga (USA, Europe) (Wii Virtual Console).zip
│   ├── [ 35K]  Valkyrie no Bouken - Toki no Kagi Densetsu (Japan) (Virtual Console).zip
│   ├── [ 20K]  Volleyball (USA, Europe) (Virtual Console).zip
│   ├── [149K]  Wagyan Land 2 (Japan) (Virtual Console).zip
│   ├── [222K]  Wai Wai World 2 - SOS!! Paseri Jou (Japan) (Virtual Console).zip
│   ├── [ 26K]  Wrecking Crew (World) (Virtual Console).zip
│   ├── [ 86K]  Yoshi's Cookie (Europe) (Virtual Console).zip
│   ├── [ 69K]  Yoshi's Cookie (USA) (Virtual Console).zip
│   ├── [125K]  Youkai Douchuuki (Japan) (Virtual Console).zip
│   ├── [ 87K]  Zelda II - The Adventure of Link (Europe) (Rev 2) (Virtual Console).zip
│   ├── [ 87K]  Zelda II - The Adventure of Link (USA) (Virtual Console).zip
│   └── [288K]  Zoda's Revenge - StarTropics II (USA, Europe) (Virtual Console).zip
├── [193K]  8BIT Rhythm Land (World) (Aftermarket) (Unl).zip
├── [313K]  Asder - 20 in 1 (USA) (Unl).zip
├── [325K]  Bao Xiao San Guo (Asia) (Unl) [b].zip
├── [ 98K]  Bugs Bunny Birthday Bash (USA) (Beta) [b].zip
├── [ 16K]  Dushlan (World) (Aftermarket) (Unl).zip
├── [ 27K]  Exerion (Japan) (En) (Proto) [b].zip
├── [ 91K]  Happily Ever After starring Snow White (USA) (Proto).zip
├── [ 66K]  Happy Camper (USA) (Proto) (Unl) [b].zip
├── [339K]  Haunted Halloween '86 (World) (Aftermarket) (Unl).zip
├── [ 36K]  Incantation (Asia) (Unl) [b].zip
├── [6.0K]  Magic Giral (Asia) (Unl).zip
├── [191K]  Mike Tyson's Intergalactic Power Punch (USA) (Beta) [b].zip
├── [143K]  NEO Heiankyo Alien (World) (Ja) (Aftermarket) (Unl).zip
├── [138K]  Sanguozhi - Chibi zhi Zhan (Asia) (Unl).zip
├── [114K]  Shuffle Fight (Japan).zip
├── [ 53K]  Star Versus (World) (Aftermarket) (Unl).zip
├── [1.7M]  Super 190-in-1 (Asia) (En) (Unl) (Pirate).zip
├── [ 26K]  Tag Team Pro-Wrestling Special (Japan) (Sample).zip
├── [ 29K]  U-Force Power Games (USA) (Proto 2) [b].zip
└── [134K]  Zhongguo Daheng (Asia) (Unl).zip

1 directory, 177 file
```